<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TestBundle\Entity\MarketPlace;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * OrderItem
 *
 * @ORM\Table(indexes={@ORM\Index(name="orderId_idx", columns={"order_id"})})
 * @ORM\Entity(repositoryClass="TestBundle\Entity\OrderItemRepository")
 * @GRID\Source(columns="id, orderId, idFlux, orderAmount, orderTax, orderShipping, marketPlace.name")
 */
class OrderItem
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="marketplace_id", type="integer", nullable=true)
     */
    protected $marketplaceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="idFlux", type="integer")
     * @Assert\Type(type="integer")
     */
    protected $idFlux;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=255)
     * @Assert\Type(type="integer")
     */
    protected $orderId;

    /**
     * @var float
     *
     * @ORM\Column(name="order_amount", type="float")
     * @Assert\Type(type="float")
     */
    protected $orderAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="order_tax", type="float")
     * @Assert\Type(type="float")     
     */
    protected $orderTax;

    /**
     * @var float
     *
     * @ORM\Column(name="order_shipping", type="float")
     * @Assert\Type(type="float")     
     */
    protected $orderShipping;

    /**
     * @ORM\ManyToOne(targetEntity="MarketPlace", inversedBy="orderItems")
     * @ORM\JoinColumn(name="marketplace_id", referencedColumnName="id")
     * @GRID\Column(field="marketPlace.name", title="marketPlace")
     * @JMS\MaxDepth(0)
     */
    protected $marketPlace;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplaceId
     *
     * @param integer $marketplaceId
     * @return OrderItem
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;

        return $this;
    }

    /**
     * Get marketplaceId
     *
     * @return integer 
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return OrderItem
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;

        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer 
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return OrderItem
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return OrderItem
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set orderTax
     *
     * @param float $orderTax
     * @return OrderItem
     */
    public function setOrderTax($orderTax)
    {
        $this->orderTax = $orderTax;

        return $this;
    }

    /**
     * Get orderTax
     *
     * @return float 
     */
    public function getOrderTax()
    {
        return $this->orderTax;
    }

    /**
     * Set orderShipping
     *
     * @param float $orderShipping
     * @return OrderItem
     */
    public function setOrderShipping($orderShipping)
    {
        $this->orderShipping = $orderShipping;

        return $this;
    }

    /**
     * Get orderShipping
     *
     * @return float 
     */
    public function getOrderShipping()
    {
        return $this->orderShipping;
    }

    /**
     * Get Order marketPlace
     *
     * @return MarketPlace
     */
    public function getMarketPlace()
    {
        return $this->marketPlace;
    }

    /**
     * Set Order marketPlace
     * 
     * @param MarketPlace
     */
    public function setMarketPlace(MarketPlace $marketPlace)
    {
        $this->marketPlace = $marketPlace;
        return $this;
    }

}
