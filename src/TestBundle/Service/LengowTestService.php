<?php

namespace TestBundle\Service;

use TestBundle\Entity\OrderItem;
use TestBundle\Entity\MarketPlace;

class LengowTestService
{

    private $logger;
    private $ordersUrl;
    private $em;

    public function __construct($logger, $ordersUrl)
    {
        $this->logger = $logger;
        $this->ordersUrl = $ordersUrl;
    }

    public function getOrders()
    {
        $this->logger->info('Call ' . $this->ordersUrl);
        try
        {
            $xmlRaw = file_get_contents($this->ordersUrl);
        }
        catch (\Exception $e)
        {
            $this->logger->error('Error file_get_contents: ' . $e->getMessage());
        }

        $this->logger->info('Call result: ');
        $this->logger->info($xmlRaw);

        $xml = new \SimpleXMLElement($xmlRaw);
        return $xml->orders->order;
    }

    public function importOrders($em)
    {
        $this->em = $em;
        $orders = $this->getOrders();
        $orderRepo = $em->getRepository('TestBundle:OrderItem');
        $nbImported = 0;
        foreach ($orders as $order)
        {
            $orderId = (string) $order->order_id;
            $orderItem = $orderRepo->findOneByOrderId($orderId);
            if (!$orderItem)
            {
                ++$nbImported;
                $this->importNewOrder($order);
            }
        }

        $this->logger->info($nbImported . ' orders imported');
        return $nbImported;
    }

    private function importNewOrder($order)
    {
        $orderItem = new OrderItem;

        $orderItem->setOrderId((string) $order->order_id);
        $orderItem->setIdFlux((int) $order->idFlux);
        $orderItem->setOrderAmount((float) $order->order_amount);
        $orderItem->setOrderTax((float) $order->order_tax);
        $orderItem->setOrderShipping((float) $order->order_shipping);

        $marketPlace = $this->em->getRepository("TestBundle:MarketPlace")->findOneByName($order->marketplace);
        if (!$marketPlace)
        {
            $marketPlace = $this->createNewMarketPlace($order->marketplace);
        }

        $orderItem->setMarketPlace($marketPlace);
        $this->em->persist($orderItem);
        $this->em->flush();
    }

    private function createNewMarketPlace($name)
    {
        $marketPlace = new MarketPlace;
        $marketPlace->setName((string) $name);
        $this->em->persist($marketPlace);
        $this->em->flush();
        return $marketPlace;
    }

}
