<?php

namespace TestBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportOrdersCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName("Test:ImportOders")
                ->setDescription("Import orders from xml")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lengowTest = $this->getContainer()->get('lengow_test');
        $orders = $lengowTest->importOrders($this->getContainer()->get('doctrine')->getManager());
        $output->writeln($orders . ' orders added');
    }

}
