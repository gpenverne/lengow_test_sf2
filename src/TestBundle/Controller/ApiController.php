<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{

    /**
     * @Route("/", requirements={"_format" = "json|yml"}, defaults={"_format": "json"})
     */
    public function apiAction($_format)
    {
        $orders = $this->getDoctrine()->getManager()->getRepository("TestBundle:OrderItem")->findAll();

        $serializer = SerializerBuilder::create()->build();
        $rawContent = $serializer->serialize(array('orders' => $orders), $_format, SerializationContext::create()->enableMaxDepthChecks());

        $response = new Response($rawContent);
        return $response;
    }

    /**
     * @Route("/{orderId}.{_format}", requirements={"orderId" = "\d+", "_format" = "json|yml"}, defaults={"_format": "json"})
     */
    public function getJsonOrder($orderId, $_format)
    {
        $order = $this->getDoctrine()->getManager()->getRepository('TestBundle:OrderItem')->findOneById($orderId);
        if (!$order)
        {
            throw $this->createNotFoundException('Order not found');
        }
        $serializer = SerializerBuilder::create()->build();
        $rawContent = $serializer->serialize(array('order' => $order), $_format, SerializationContext::create()->enableMaxDepthChecks());

        $response = new Response($rawContent);
        return $response;
    }

}
