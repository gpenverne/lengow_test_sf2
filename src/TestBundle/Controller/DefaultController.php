<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use TestBundle\Entity\OrderItem;
use APY\DataGridBundle\Grid\Source\Entity;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function homeAction()
    {
        $lengowTest = $this->container->get('lengow_test');
        $lengowTest->importOrders($this->getDoctrine()->getManager());

        $grid = $this->get('grid');
        $source = new Entity('TestBundle:OrderItem');
        $grid->setSource($source);

        return $grid->getGridResponse('TestBundle:Default:home.html.twig');
    }

    /**
     * @Route("/addOrder", name="addOrder")
     * @Template()
     */
    public function addOrderAction()
    {
        $em = $this->getDoctrine()->getManager();
        $orderItem = new OrderItem;
        $form = $this->createFormBuilder($orderItem)
                ->add('idFlux', 'text')
                ->add('orderId', 'text')
                ->add('orderAmount', 'text')
                ->add('orderTax', 'text')
                ->add('orderShipping', 'text')
                ->add('marketPlace', 'entity', array(
                    'class' => 'TestBundle:MarketPlace',
                    'query_builder' => $em->getRepository('TestBundle:MarketPlace')->createQueryBuilder('m')->orderBy('m.name', 'ASC'),
                    'property' => 'name',
                ))
                ->add('save', 'submit', array('label' => 'Save orderItem'))
                ->getForm();

        if ($this->getRequest()->getMethod() == 'POST')
        {
            $form->handleRequest($this->getRequest());
            if ($form->isValid())
            {
                $em->persist($orderItem);
                $em->flush();
                return $this->redirect($this->generateUrl('home'));
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

}
